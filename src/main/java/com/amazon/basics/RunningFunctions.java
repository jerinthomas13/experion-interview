package com.amazon.basics;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface RunningFunctions {
	
	void loadObjects() throws FileNotFoundException, IOException;
	boolean launchApp(String appPackage,String appActivity,String udid, String deviceName, String version,String platformname);
	void scrollToDown();
}
