package com.amazon.basics;

import org.testng.annotations.DataProvider;

import com.amazon.utils.DataProviderInput;

import io.appium.java_client.android.AndroidDriver;



public class AmazonFuntions extends Functions {
	
	protected String dataSheetName;
	
	@DataProvider(name="fetchData")
	public Object[][] getData(){
		return DataProviderInput.getSheet(dataSheetName);		
	}


}
