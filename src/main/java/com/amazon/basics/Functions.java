package com.amazon.basics;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Properties;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;


public class Functions implements RunningFunctions {
	
	protected AndroidDriver<?> driver;
	protected static Properties prop;
	
	public void loadObjects() throws FileNotFoundException, IOException
	{
		prop = new Properties();
		try {
			prop.load(new FileInputStream(new File("./config.properties")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public boolean launchApp(String deviceName,String udid, String platformname,String version,String appPackage,String appActivity )
	{
		try {
			
			System.out.println("Launch App in wrapper class");
			DesiredCapabilities dc = new DesiredCapabilities();
			dc.setCapability("appPackage", appPackage);
			dc.setCapability("appActivity", appActivity);
			System.out.println(appActivity+"functions");
			dc.setCapability("udid", udid);
			dc.setCapability("deviceName", deviceName);
			dc.setCapability("platformName", platformname);
			dc.setCapability("version",version);
			dc.setCapability("noReset", "true");
			driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), dc);

		} 
		catch (MalformedURLException e) {
			e.printStackTrace();

		}
		return true;	
	}
	
	public void scrollToDown()
	{
		
	    int height = driver.manage().window().getSize().height;
	    int width = driver.manage().window().getSize().width;
	    Dimension size=driver.manage().window().getSize();
	    int startx=size.getWidth();
	    int starty=(int)(size.getHeight()*0.8);
	    int endy=(int)(size.getHeight()*0.2);
	    TouchAction action = new TouchAction(driver);
	    action.press(PointOption.point(width / 2, height / 2))
	    .moveTo(PointOption.point(startx,starty)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(1200)))
	    .moveTo(PointOption.point(startx,endy))
	    .release()
	    .perform();
		 
	}
	
	

}
