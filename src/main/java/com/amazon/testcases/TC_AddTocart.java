package com.amazon.testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.amazon.basics.AmazonFuntions;
import com.amazon.pages.ListingPage;

public class TC_AddTocart extends AmazonFuntions{
	
	@BeforeClass
	public void values(){
		dataSheetName = "Cart";
	}
	@Test(dataProvider = "fetchData")
	
	public void addcart(String searchText,String searchResult,String televisionName)
	{
		System.out.println("add to cart class");
		new ListingPage()
		.clickOnSearch(searchText)
		.clickOnASearchResult(searchResult)
		.selectATV(televisionName);
	}

}
