package com.amazon.testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.amazon.basics.AmazonFuntions;
import com.amazon.pages.LaunchAppPage;

public class TC_LaunchApp extends AmazonFuntions {


	@BeforeClass
	public void values(){
		dataSheetName = "LaunchApp";
	}
	
	
	@Test(dataProvider = "fetchData")
	public void launchApplication(String deviceName,String udid, String platformname,String version,String appPackage,String appActivity) throws Exception 
	{
		System.out.println("Data provoder class");
		new LaunchAppPage()
		.launchAmazon(deviceName, udid, platformname, version, appPackage, appActivity);
		
		
	}
}
