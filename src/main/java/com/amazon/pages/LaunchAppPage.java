package com.amazon.pages;


import com.amazon.basics.AmazonFuntions;


public class LaunchAppPage extends AmazonFuntions {
	

	public LaunchAppPage launchAmazon(String deviceName,String udid, String platformname,String version,String appPackage,String appActivity)
			throws InterruptedException {
		System.out.println("Launch App Page");
		
		launchApp(deviceName,udid,platformname,version,appPackage,appActivity);
		return this;
	}

}
