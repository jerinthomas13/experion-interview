package com.amazon.pages;



import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;

import com.amazon.basics.Functions;

import io.appium.java_client.android.AndroidDriver;

public class ListingPage extends Functions {
	
	
	public ListingPage clickOnSearch(String searchText)
	{
		System.out.println(searchText+":The search Text is");
		driver.findElement(By.xpath("//android.widget.EditText[@resource-id='in.amazon.mShop.android.shopping:id/rs_search_src_text']")).click();
		driver.findElement(By.xpath("//android.widget.EditText[@resource-id='in.amazon.mShop.android.shopping:id/rs_search_src_text']")).sendKeys(searchText);
		return this;
		
	}
	
	public ListingPage clickOnASearchResult(String searchResult)
	{
		
		System.out.println(searchResult+"ClickOnsearchResult");
		WebElement clickSearchResult=driver.findElement(By.xpath("(//android.widget.TextView[contains(text(),'')])[10]"));
		clickSearchResult.click();
		return this;
	}
	
	
	public ListingPage selectATV(String televisionName)
	{
		System.out.println(televisionName);
		scrollToDown();
		driver.findElement(By.xpath("//android.view.View[contains(text(),'LG 108 cm (43 inches) 4K Ultra HD Smart LED TV 43UM7290PTF (Ceramic Black)')]")).click();	
		return this;
		
	}

}
