package com.amazon.pages;

import org.openqa.selenium.By;


import com.amazon.basics.Functions;

import io.appium.java_client.android.AndroidDriver;

public class LoginPage extends Functions{

	public LoginPage(AndroidDriver<?> driver) {
		this.driver=driver;
		
	}
	public LoginPage clickOnSignIn()
	{
		driver.findElement(By.xpath("//android.view.View[@resource-id='gwm-SignIn-button']")).click();
		
		return this;
		
	}
	
	public LoginPage enterUserName(String username)
	{
		return this;
	}
	
	public LoginPage enterPassword(String Password)
	{
		return this;
	}
	
	public LoginPage clickToLogin()
	{
		return this;
	}
	
}
